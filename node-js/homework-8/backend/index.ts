import App from "./src/App";
import Container from "typedi";
import NewsController from "./src/controllers/NewsController";
import config from "./src/utils/convictConfig";

const main = async () => {
  const port = config.get('PORT');
  const host = config.get('HOST');

  const app = new App([Container.get(NewsController)], port, host);
  
  app.listen();
};

main().catch((error) => {
  console.error(error);
});
