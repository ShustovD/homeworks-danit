"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const newsPostSchema = {
    type: 'object',
    properties: {
        id: { type: 'string' },
        title: { type: 'string', maxLength: 50 },
        text: { type: 'string', maxLength: 256 },
        genre: { type: 'string', enum: ['Politic', 'Business', 'Sport', 'Other'] },
        isPrivate: { type: 'boolean' },
        createdAt: { type: 'string' },
    },
    additionalProperties: false,
    required: ['title', 'text', 'genre', 'isPrivate'],
};
exports.default = newsPostSchema;
