import {JSONSchemaType} from "ajv"
import { IPost } from "../../interfaces";

const newsPostSchema: JSONSchemaType<IPost> = {
    type: 'object',
    properties: {
        id: { type: 'string' },
        title: { type: 'string', maxLength: 50 },
        text: { type: 'string', maxLength: 256 },
        genre: { type: 'string', enum: ['Politic', 'Business', 'Sport', 'Other'] },
        isPrivate: { type: 'boolean' },
        createdAt: { type: 'string'},
    },
    additionalProperties: false,
    required: [ 'title', 'text', 'genre', 'isPrivate'],
};

export default newsPostSchema;
  