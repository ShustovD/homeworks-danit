import { Service } from "typedi";
import FileDB from "../dal/FileDB";
import { IPageOptions, IPagedPosts, IPost } from "../../interfaces";
import newsPostSchema from "../models/newsPostsSchema";

@Service()
class NewsPostsService {
    
    constructor(private fileDB: FileDB) {
        this.initializeSchema();
    }
    
    private async initializeSchema() {
        this.fileDB = await FileDB.registerSchema('newsposts', newsPostSchema)
        this.fileDB = await FileDB.getTable('newsposts'); 
    }

    getAll = (): Promise<IPost[]> => {
        return this.fileDB.getAll()
    }

    getAllPostsPaged = (params: IPageOptions): Promise<IPagedPosts> => {
        return this.fileDB.getAllPostsPaged(params)
    }

    getById = (id: string): Promise<IPost> => {
        return this.fileDB.getById(id)
    }

    create = (field:Omit<IPost, "id" | "createdAt">): Promise<IPost> => {
        return this.fileDB.create(field)
    }

    update = (id: string, updatedFields: Omit<IPost, "id" | "createdAt">): Promise<IPost> => {
        return this.fileDB.update(id, updatedFields)
    }

    delete = (id:string): Promise<IPost> => {
        return this.fileDB.delete(id)
    }
}

export default NewsPostsService;