import './Modal.css'

function Modal({onClick, onClose, header, text, closeButton, actions}: IModal) {

    const handleModalClick = (event: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
        if (event.target === event.currentTarget) {
            onClick(); 
        }
    };

    return (
        <div className="modal" onClick={(event) => handleModalClick(event)}>
            <div className="modal__header">
                <h1 className="modal__header-title">{header}</h1>
                {closeButton && (
                    <button className="modal__header-close" onClick={onClose}>
                        &times;
                    </button>
                )}
            </div>
            <div className="modal__main">
                <p className="modal__main-content">{text}</p>
            </div>
            <div className="modal__actions">
                {actions}
            </div>
        </div>
    );
  }

export default Modal