interface IPost {
    id?: string;
    title: string;
    text: string;
    genre: 'Politic' | 'Business' | 'Sport' | 'Other',
    isPrivate: boolean, 
    createdAt?: string;
}


interface IButton {
    className?: string;
    children: React.ReactNode;
    onClick?: () => void;
    type?: "button" | "reset" | "submit";
}

interface IModal {
    onClick: () => void;
    onClose: () => void;
    header: string;
    text: string;
    closeButton: boolean;
    actions: ReactNode;
}

interface IPagination {
    currentPage: number;
    lastPage: number;
    maxLength: number;
    setCurrentPage: (page: number) => void;
}

interface IPageOptions {
    size: number;
    page: number;
}

interface IPagedPosts {
    pageOptions: IPageOptions;
    total: number;
    results: IPost[];
}