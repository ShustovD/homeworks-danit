import { Request } from "express";

export interface IPost {
    id: string;
    title: string;
    text: string;
    createdAt: Date;
}

export interface IPageOptions {
    size: number;
    page: number;
}

export interface IPagedPosts {
    pageOptions: IPageOptions;
    total: number;
    results: IPost[];
}
  
export interface ExtRequest extends Request {
    pageOptions?: IPageOptions;
  }
  