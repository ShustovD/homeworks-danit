import { IPost } from "../../interfaces";

const newsPostSchema: IPost = {
    id: '',
    title: '',
    text: '',
    createdAt: new Date(),
};

export {newsPostSchema};
  