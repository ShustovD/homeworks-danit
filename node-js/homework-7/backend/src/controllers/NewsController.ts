import express from 'express'
import { Service } from 'typedi';
import NewsPostsService from '../bll/NewsPosts.service';
import { ExtRequest } from '../../interfaces';
import pageOptionsMiddleware from '../middleware/pageOptionsMiddleware';

@Service()
class NewsController {
  public router:  express.Router;

  constructor(private newsPostsService: NewsPostsService) {
    this.router = express.Router(); 
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.get('/newsposts', pageOptionsMiddleware(), this.getAllPostsQuery);
    this.router.get('/newsposts/:id', this.getPostById);
    this.router.post('/newsposts', this.createPost);
    this.router.put('/newsposts/:id', this.updatePost);
    this.router.delete('/newsposts/:id', this.deletePost);
  }
  
  getAllPosts = async (_req: express.Request, res: express.Response) => {
      try {
        const posts = await this.newsPostsService.getAll()
        res.send(posts);
      } catch (error) {
        res.status(500).send({ error: 'An error occurred while fetching posts.' });
      }
  };

  getAllPostsQuery = async (req: ExtRequest, res: express.Response) => {
    try {
     const params = req.pageOptions;
     if (!params) {
      return res.status(404).send({ error: 'Params not found.' });
    }
     const posts = await this.newsPostsService.getAllPostsPaged(params);
     res.send(posts);
    } catch (error) {
     res.status(500).send({ error: 'An error occurred while fetching posts.' });
    }
   };

  getPostById = async  (req: express.Request, res: express.Response) => {
      try {
        const post = await this.newsPostsService.getById(req.params.id);
        if (!post) {
          return res.status(404).send({ error: 'Post not found.' });
        }
        res.send(post);
      } catch (error) {
        res.status(500).send({ error: 'Internal Server Error' });
      }
  };

  createPost = async  (req: express.Request, res: express.Response) => {
      try {
        const newPost = await this.newsPostsService.create(req.body);
        console.log(newPost);
        res.send(newPost);
      } catch (error) {
        res.status(500).send({ error: 'An error occurred while creating the post.' });
      }
  };

  updatePost = async  (req: express.Request, res: express.Response) => {
      try {
        const updatedPost = await this.newsPostsService.update(req.params.id, req.body);
        if (!updatedPost) {
          return res.status(404).send({ error: 'Post not found.' });
        }
        res.send(updatedPost);
      } catch (error) {
        res.status(500).send({ error: 'An error occurred while updating the post.' });
      }
  };

  deletePost = async  (req: express.Request, res: express.Response) => {
      try {
          const deletedPost = await this.newsPostsService.delete(req.params.id);
          if (!deletedPost) {
            return res.status(404).send({ error: 'Post not found.' });
          }
          res.send(deletedPost);
      } catch (error) {
          res.status(500).send({ error: 'An error occurred while deleting the post.' });
      }
  }
}

export default NewsController