import 'reflect-metadata';
import bodyParser from "body-parser";
import cors from "cors";
import express from "express";

class App {
    public app: express.Application;
    public port: number;
    public host: string;
  
    constructor(controllers: any[], port: number, host:string) {
      this.app = express();
      this.port = port;
      this.host = host;
  
      this.initializeMiddlewares();
      this.initializeControllers(controllers);
    }
  
    private initializeMiddlewares() {
      this.app.use(cors());
      this.app.use(bodyParser.json());
      this.app.use(express.static('public'));
    }
  
    private initializeControllers(controllers: any[]) {
      controllers.forEach((controller) => {
        this.app.use('/', controller.router);
      });
    }
  
    public listen() {
      this.app.listen(this.port, () => {
        console.log(`App listening on http://${this.host}:${this.port}`);
      });
    }
}

export default App;