"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const typedi_1 = require("typedi");
const NewsPosts_service_1 = __importDefault(require("../bll/NewsPosts.service"));
const pageOptionsMiddleware_1 = __importDefault(require("../middleware/pageOptionsMiddleware"));
let NewsController = class NewsController {
    constructor(newsPostsService) {
        this.newsPostsService = newsPostsService;
        this.getAllPosts = async (_req, res) => {
            try {
                const posts = await this.newsPostsService.getAll();
                res.send(posts);
            }
            catch (error) {
                res.status(500).send({ error: 'An error occurred while fetching posts.' });
            }
        };
        this.getAllPostsQuery = async (req, res) => {
            try {
                const params = req.pageOptions;
                if (!params) {
                    return res.status(404).send({ error: 'Params not found.' });
                }
                const posts = await this.newsPostsService.getAllPostsPaged(params);
                res.send(posts);
            }
            catch (error) {
                res.status(500).send({ error: 'An error occurred while fetching posts.' });
            }
        };
        this.getPostById = async (req, res) => {
            try {
                const post = await this.newsPostsService.getById(req.params.id);
                if (!post) {
                    return res.status(404).send({ error: 'Post not found.' });
                }
                res.send(post);
            }
            catch (error) {
                res.status(500).send({ error: 'Internal Server Error' });
            }
        };
        this.createPost = async (req, res) => {
            try {
                const newPost = await this.newsPostsService.create(req.body);
                console.log(newPost);
                res.send(newPost);
            }
            catch (error) {
                res.status(500).send({ error: 'An error occurred while creating the post.' });
            }
        };
        this.updatePost = async (req, res) => {
            try {
                const updatedPost = await this.newsPostsService.update(req.params.id, req.body);
                if (!updatedPost) {
                    return res.status(404).send({ error: 'Post not found.' });
                }
                res.send(updatedPost);
            }
            catch (error) {
                res.status(500).send({ error: 'An error occurred while updating the post.' });
            }
        };
        this.deletePost = async (req, res) => {
            try {
                const deletedPost = await this.newsPostsService.delete(req.params.id);
                if (!deletedPost) {
                    return res.status(404).send({ error: 'Post not found.' });
                }
                res.send(deletedPost);
            }
            catch (error) {
                res.status(500).send({ error: 'An error occurred while deleting the post.' });
            }
        };
        this.router = express_1.default.Router();
        this.initializeRoutes();
    }
    initializeRoutes() {
        this.router.get('/newsposts', (0, pageOptionsMiddleware_1.default)(), this.getAllPostsQuery);
        this.router.get('/newsposts/:id', this.getPostById);
        this.router.post('/newsposts', this.createPost);
        this.router.put('/newsposts/:id', this.updatePost);
        this.router.delete('/newsposts/:id', this.deletePost);
    }
};
NewsController = __decorate([
    (0, typedi_1.Service)(),
    __metadata("design:paramtypes", [NewsPosts_service_1.default])
], NewsController);
exports.default = NewsController;
