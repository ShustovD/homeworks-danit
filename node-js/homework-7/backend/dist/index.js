"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const App_1 = __importDefault(require("./src/App"));
const typedi_1 = __importDefault(require("typedi"));
const config_1 = __importDefault(require("./src/config"));
const NewsController_1 = __importDefault(require("./src/controllers/NewsController"));
const main = async () => {
    const port = config_1.default.get('PORT');
    const host = config_1.default.get('HOST');
    const app = new App_1.default([typedi_1.default.get(NewsController_1.default)], port, host);
    app.listen();
};
main().catch((error) => {
    console.error(error);
});
