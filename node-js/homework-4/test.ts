import Card from "./src/Card";
import BonusCard from "./src/BonusCard";
import Pocket from "./src/Pocket";
import { Transaction, CurrencyEnum } from "./src/Transaction";

const card = new Card(1000, CurrencyEnum.USD);
const bonusCard = new BonusCard(100, CurrencyEnum.UAH);

const transaction1 = new Transaction(100, CurrencyEnum.USD);
const transaction2 = new Transaction(200, CurrencyEnum.UAH);
const transaction3 = new Transaction(400, CurrencyEnum.UAH);

card.AddTransaction(transaction1);
card.AddTransaction(transaction2);

bonusCard.AddTransaction(transaction3);

const pocket = new Pocket();
// Create cards
pocket.AddCard('Card', card);
pocket.AddCard('Bonus Card', bonusCard);

// Get cards 
const getCard = pocket.GetCard('Card');
console.log('Card:', getCard);
const getBonusCard = pocket.GetCard('Bonus Card');
console.log('Bonus Card:', getBonusCard);

// Get total amount in USD
const totalAmountUSD = pocket.GetTotalAmount(CurrencyEnum.USD);
console.log('Total amount in USD:', totalAmountUSD);

// Get total amount in UAH
const totalAmountUAH = pocket.GetTotalAmount(CurrencyEnum.UAH);
console.log('Total amount in UAH:', totalAmountUAH);

try {
    pocket.RemoveCard('BonusCard');
    console.log('Bonus card was removed');
} catch (error) {
    console.error(error.message);
}

try {
    pocket.RemoveCard('Bonus Card');
    console.log('Bonus card was removed');
} catch (error) {
    console.error(error.message);
}
