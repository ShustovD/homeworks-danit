import { CurrencyEnum, Transaction } from "./src/Transaction";

interface ICard {
    AddTransaction(Transaction: Transaction): string;
    AddTransaction(Currency: CurrencyEnum, Amount: number): string;
    AddTransaction(a: Transaction | CurrencyEnum, b?: number): string;
    GetTransaction(Id: string): Transaction;
    GetBalance(Currency: CurrencyEnum): number;
  }
  