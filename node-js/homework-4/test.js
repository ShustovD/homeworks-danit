"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Card_1 = require("./src/Card");
var BonusCard_1 = require("./src/BonusCard");
var Pocket_1 = require("./src/Pocket");
var Transaction_1 = require("./src/Transaction");
var card = new Card_1.default(1000, Transaction_1.CurrencyEnum.USD);
var bonusCard = new BonusCard_1.default(100, Transaction_1.CurrencyEnum.UAH);
var transaction1 = new Transaction_1.Transaction(100, Transaction_1.CurrencyEnum.USD);
var transaction2 = new Transaction_1.Transaction(200, Transaction_1.CurrencyEnum.UAH);
var transaction3 = new Transaction_1.Transaction(400, Transaction_1.CurrencyEnum.UAH);
card.AddTransaction(transaction1);
card.AddTransaction(transaction2);
bonusCard.AddTransaction(transaction3);
var pocket = new Pocket_1.default();
// Create cards
pocket.AddCard('Card', card);
pocket.AddCard('Bonus Card', bonusCard);
// Get cards 
var getCard = pocket.GetCard('Card');
console.log('Card:', getCard);
var getBonusCard = pocket.GetCard('Bonus Card');
console.log('Bonus Card:', getBonusCard);
// Get total amount in USD
var totalAmountUSD = pocket.GetTotalAmount(Transaction_1.CurrencyEnum.USD);
console.log('Total amount in USD:', totalAmountUSD);
// Get total amount in UAH
var totalAmountUAH = pocket.GetTotalAmount(Transaction_1.CurrencyEnum.UAH);
console.log('Total amount in UAH:', totalAmountUAH);
try {
    pocket.RemoveCard('BonusCard');
    console.log('Bonus card was removed');
}
catch (error) {
    console.error(error.message);
}
try {
    pocket.RemoveCard('Bonus Card');
    console.log('Bonus card was removed');
}
catch (error) {
    console.error(error.message);
}
