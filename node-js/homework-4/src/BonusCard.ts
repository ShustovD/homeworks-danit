import { ICard } from "../interfaces";
import { CurrencyEnum, Transaction } from "./Transaction";

class BonusCard extends Transaction implements ICard {
    transactions: Transaction[] = [];
  
    AddTransaction(Transaction: Transaction): string;
    AddTransaction(Currency: CurrencyEnum, Amount: number): string;
  
    AddTransaction(a: Transaction | CurrencyEnum, b?: number): string {
        let basicAmount = 0
        if (a instanceof Transaction) {
            this.transactions.push(a);
            basicAmount = a.Amount || 0
        } else if (typeof a === "number" && b !== undefined) {
            basicAmount = b
            const basicTransaction = new Transaction(b, a);
            this.transactions.push(basicTransaction);
           
        } else {
            throw new Error(" Error");
        }

        const bonusAmount = basicAmount * 0.1
        const bonusTransaction = new Transaction(bonusAmount, a instanceof Transaction ? a.Currency : a);
        this.transactions.push(bonusTransaction);
        return bonusTransaction.Id;
    }
    
    GetTransaction(Id: string): Transaction  {
        const transaction =  this.transactions.find(transaction => transaction.Id === Id);
        if(!transaction){
            throw new Error("Transaction not found")
        }
        return transaction
    }
  
    GetBalance(Currency: CurrencyEnum): number   {
        return this.transactions.reduce((total, transaction) => {
            if (transaction.Currency === Currency) {
            return total + (transaction.Amount || 0);
            }
            return total;
        }, 0);
    }
}

export default BonusCard;