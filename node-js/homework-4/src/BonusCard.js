"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Transaction_1 = require("./Transaction");
var BonusCard = /** @class */ (function (_super) {
    __extends(BonusCard, _super);
    function BonusCard() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.transactions = [];
        return _this;
    }
    BonusCard.prototype.AddTransaction = function (a, b) {
        var basicAmount = 0;
        if (a instanceof Transaction_1.Transaction) {
            this.transactions.push(a);
            basicAmount = a.Amount || 0;
        }
        else if (typeof a === "number" && b !== undefined) {
            basicAmount = b;
            var basicTransaction = new Transaction_1.Transaction(b, a);
            this.transactions.push(basicTransaction);
        }
        else {
            throw new Error(" Error");
        }
        var bonusAmount = basicAmount * 0.1;
        var bonusTransaction = new Transaction_1.Transaction(bonusAmount, a instanceof Transaction_1.Transaction ? a.Currency : a);
        this.transactions.push(bonusTransaction);
        return bonusTransaction.Id;
    };
    BonusCard.prototype.GetTransaction = function (Id) {
        var transaction = this.transactions.find(function (transaction) { return transaction.Id === Id; });
        if (!transaction) {
            throw new Error("Transaction not found");
        }
        return transaction;
    };
    BonusCard.prototype.GetBalance = function (Currency) {
        return this.transactions.reduce(function (total, transaction) {
            if (transaction.Currency === Currency) {
                return total + (transaction.Amount || 0);
            }
            return total;
        }, 0);
    };
    return BonusCard;
}(Transaction_1.Transaction));
exports.default = BonusCard;
