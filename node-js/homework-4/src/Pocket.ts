import { ICard } from "../interfaces";
import { CurrencyEnum } from "./Transaction";

class Pocket {
    private cards:Map<string, ICard> = new Map()
    AddCard(Name: string, Card: ICard): ICard {
        this.cards.set(Name, Card);
        return Card;
    }

    RemoveCard(Name:string): void  {
        if (this.cards.has(Name)) {
            this.cards.delete(Name);
        } else {
            throw new Error('Card not found!');
        }
    }

    GetCard(Name:string): ICard {
       const card = this.cards.get(Name)
       if(!card) {
        throw new Error('Card not found!')
       }
       return card
    }

    GetTotalAmount(Currency: CurrencyEnum): number {
        let totalAmount = 0;
    
        this.cards.forEach((card) => {
            totalAmount += card.GetBalance(Currency);
        });
        return totalAmount;
    }
}

export default Pocket
