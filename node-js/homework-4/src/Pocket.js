"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Pocket = /** @class */ (function () {
    function Pocket() {
        this.cards = new Map();
    }
    Pocket.prototype.AddCard = function (Name, Card) {
        this.cards.set(Name, Card);
        return Card;
    };
    Pocket.prototype.RemoveCard = function (Name) {
        if (this.cards.has(Name)) {
            this.cards.delete(Name);
        }
        else {
            throw new Error('Card not found!');
        }
    };
    Pocket.prototype.GetCard = function (Name) {
        var card = this.cards.get(Name);
        if (!card) {
            throw new Error('Card not found!');
        }
        return card;
    };
    Pocket.prototype.GetTotalAmount = function (Currency) {
        var totalAmount = 0;
        this.cards.forEach(function (card) {
            totalAmount += card.GetBalance(Currency);
        });
        return totalAmount;
    };
    return Pocket;
}());
exports.default = Pocket;
