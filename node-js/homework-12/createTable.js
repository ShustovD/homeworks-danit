import connectToDatabase from './connection.js';

const client = await connectToDatabase();

try {
    await client.connect();
    const query = `
    CREATE TABLE postgre.videos  (
        id SERIAL PRIMARY KEY,
        title TEXT,
        views FLOAT4,
        category TEXT
    )`;

    await client.query(query);

    console.log('Table created');
} catch (error) {
    console.log(error);
} finally {
    await client.end();
}
