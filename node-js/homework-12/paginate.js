import yargs from 'yargs';
import connectToDatabase from './connection.js';

const client = await connectToDatabase();
async function paginate(page, size) {
    try {
        await client.connect();
        const offset = (page - 1) * size;
        const query = `SELECT * FROM postgre.videos LIMIT $1 OFFSET $2`;
        const result = await client.query(query, [size, offset]);

        console.log(result.rows);
    } catch (error) {
        console.log(error);
    } finally {
        await client.end();
    }
}

const argv = yargs(process.argv.slice(2))
    .command('paginate', 'Paginate videos', (yargs) => {
        yargs
            .option('page', {
                alias: 'p',
                describe: 'Page number',
                type: 'number',
                default: 1,
            })
            .option('size', {
                alias: 's',
                describe: 'Page size',
                type: 'number',
                default: 10,
            });
    })
    .help().argv;

const { page, size } = argv;

if (argv._[0] === 'paginate') {
    paginate(page, size);
} else {
    console.log('Invalid command. Please use: node script.js paginate --page=1 --size=10');
}
