import yargs from 'yargs';
import connectToDatabase from './connection.js';

async function find(category) {
    const client = await connectToDatabase();

    try {
        await client.connect();

        const query = `SELECT * FROM postgre.videos WHERE category LIKE $1`;
        const result = await client.query(query, [category]);

        console.log(result.rows);
    } catch (error) {
        console.error('Error:', error);
    } finally {
        await client.end();
    }
}

const argv = yargs(process.argv.slice(2))
    .command('find', 'find videos by category', (yargs) => {
        yargs.options({
            category: {
                describe: 'Find videos by category',
                demandOption: true,
                type: 'string',
            },
        });
    })
    .help().argv;

const { category } = argv;

if (argv._[0] === 'find') {
    find(category);
} else {
    console.log("Invalid command. Please use: node find.js find --category='name'");
}
