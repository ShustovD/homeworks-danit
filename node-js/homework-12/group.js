import yargs from 'yargs';
import connectToDatabase from './connection.js';

async function group() {
    const client = await connectToDatabase();

    try {
        await client.connect();

        const query = `
            SELECT category, SUM(views) AS views
            FROM postgre.videos
            GROUP BY category
        `;

        const result = await client.query(query);
        console.log('Category:');
        result.rows.forEach((row, index) => {
            console.log(`${index + 1}. ${row.category} - ${row.views} views`);
        });
    } catch (error) {
        console.error('Error:', error);
    } finally {
        await client.end();
    }
}

const argv = yargs(process.argv.slice(2)).command('group', 'group videos by category and show views').help().argv;

if (argv._[0] === 'group') {
    group();
} else {
    console.log('Invalid command. Please use: node script.js group');
}
