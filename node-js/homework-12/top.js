import yargs from 'yargs';
import connectToDatabase from './connection.js';

async function topCategories(topN) {
    const client = await connectToDatabase();

    try {
        await client.connect();

        const query = `
            SELECT category, SUM(views) AS views
            FROM postgre.videos
            GROUP BY category
            ORDER BY views DESC
            LIMIT $1
        `;

        const result = await client.query(query, [topN]);

        console.log(`Top ${topN} categories by views:`);
        result.rows.forEach((row, index) => {
            console.log(`${index + 1}. ${row.category} - ${row.views} views`);
        });
    } catch (error) {
        console.error('Error:', error);
    } finally {
        await client.end();
    }
}

const argv = yargs(process.argv.slice(2))
    .option('top', {
        describe: 'Number of top categories to display',
        demandOption: true,
        type: 'number',
    })
    .help().argv;

const { top } = argv;

if (top) {
    topCategories(top);
} else {
    console.log('Invalid command. Please use: node top.js --top=5');
}
