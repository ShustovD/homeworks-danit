import pg from 'pg';

async function connectToDatabase() {
    const client = new pg.Client({
        user: 'postgres',
        host: 'localhost',
        database: 'postgres',
        password: '55236',
        port: 5432,
    });

    return client;
}

export default connectToDatabase;
