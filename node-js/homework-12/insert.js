import yargs from 'yargs';
import connectToDatabase from './connection.js';

async function insert(title, views, category) {
    const client = await connectToDatabase();

    try {
        await client.connect();

        const query = `INSERT INTO postgre.videos (title, views, category) VALUES ($1, $2, $3)`;
        await client.query(query, [title, views, category]);

        console.log('Video added');
    } catch (error) {
        console.log(error);
    } finally {
        await client.end();
    }
}

const argv = yargs(process.argv.slice(2))
    .command('insert', 'insert a video', (yargs) => {
        yargs.options({
            title: {
                describe: 'New title for the video',
                demandOption: true,
                type: 'string',
            },
            views: {
                describe: 'Views on video',
                demandOption: true,
                type: 'number',
            },
            category: {
                describe: 'New category for the video',
                demandOption: true,
                type: 'string',
            },
        });
    })
    .help().argv;

const { title, views, category } = argv;

if (argv._[0] === 'insert') {
    insert(title, views, category);
} else {
    console.log("Invalid command. Please use: node script.js insert --title='New title' --views=000 --category='New category'");
}
