import connectToDatabase from './connection.js';

const client = await connectToDatabase();

try {
    await client.connect();
    const query = `
    CREATE TABLE postgre.newsposts (
        id SERIAL PRIMARY KEY,
        title VARCHAR(60),
        text TEXT,
        created_date TIMESTAMPTZ
    )`;

    await client.query(query);

    console.log('Table created');
} catch (error) {
    console.log(error);
} finally {
    await client.end();
}
