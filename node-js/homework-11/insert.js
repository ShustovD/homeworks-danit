import yargs from 'yargs';
import connectToDatabase from './connection.js';

async function insert(title, text) {
    const client = await connectToDatabase();
    const tsp = new Date().toISOString();

    try {
        await client.connect();

        const query = `INSERT INTO postgre.newsposts (title, text, created_date) VALUES ($1, $2, '${tsp}')`;
        await client.query(query, [title, text]);

        console.log('Newspost added');
    } catch (error) {
        console.log(error);
    } finally {
        await client.end();
    }
}

const argv = yargs(process.argv.slice(2))
    .command('insert', 'insert a newsposts', (yargs) => {
        yargs.options({
            title: {
                describe: 'New title for the newsposts',
                demandOption: true,
                type: 'string',
            },
            text: {
                describe: 'New text  for the newsposts',
                demandOption: true,
                type: 'string',
            },
        });
    })
    .help().argv;

const { title, text } = argv;

if (argv._[0] === 'insert') {
    insert(title, text);
} else {
    console.log("Invalid command. Please use: node script.js insert --title='New title' --text='New text'");
}
