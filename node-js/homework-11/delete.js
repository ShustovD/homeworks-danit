import yargs from 'yargs';
import connectToDatabase from './connection.js';

async function deleteById(id) {
    const client = await connectToDatabase();

    try {
        await client.connect();

        const query = `DELETE FROM postgre.newsposts WHERE id = ${id}`;
        await client.query(query);

        console.log('Newspost deleted');
    } catch (error) {
        console.log(error);
    } finally {
        await client.end();
    }
}

const argv = yargs(process.argv.slice(2))
    .command('delete', 'Delete a newspost', (yargs) => {
        yargs.options({
            id: {
                describe: 'Delete a newspost by id',
                demandOption: true,
                type: 'number',
            },
        });
    })
    .help().argv;

const { id } = argv;

if (argv._[0] === 'delete') {
    deleteById(id);
} else {
    console.log('Invalid command. Please use: node script.js delete --id=id');
}
