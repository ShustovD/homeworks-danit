import connectToDatabase from './connection.js';

const client = await connectToDatabase();

try {
    await client.connect();

    const query = 'SELECT * FROM postgre.newsposts';
    const result = await client.query(query);

    console.log(result.rows);
} catch (error) {
    console.log(error);
} finally {
    await client.end();
}
