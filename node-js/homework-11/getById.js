import yargs from 'yargs';
import connectToDatabase from './connection.js';

async function getNewspost(id) {
    const client = await connectToDatabase();

    try {
        await client.connect();

        const query = `SELECT * FROM postgre.newsposts WHERE id = ${id}`;
        const result = await client.query(query);

        console.log(result.rows);
    } catch (error) {
        console.log(error);
    } finally {
        await client.end();
    }
}

const argv = yargs(process.argv.slice(2))
    .command('getNewspost', 'Get a newspost', (yargs) => {
        yargs.options({
            id: {
                describe: 'Get a newspost by id',
                demandOption: true,
                type: 'number',
            },
        });
    })
    .help().argv;

const { id } = argv;

if (argv._[0] === 'getNewspost') {
    getNewspost(id);
} else {
    console.log('Invalid command. Please use: node script.js getNewspost --id=id');
}
