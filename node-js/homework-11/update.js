import yargs from 'yargs';
import connectToDatabase from './connection.js';

async function update(id, title, text) {
    const client = await connectToDatabase();

    try {
        await client.connect();

        const query = `UPDATE postgre.newsposts SET title = $2, text = $3 WHERE id = $1`;
        await client.query(query, [id, title, text]);

        console.log('Newspost updated');
    } catch (error) {
        console.log(error);
    } finally {
        await client.end();
    }
}

const argv = yargs(process.argv.slice(2))
    .command('update', 'Update a newspost', (yargs) => {
        yargs.options({
            id: {
                describe: 'Update a newspost with id',
                demandOption: true,
                type: 'number',
            },
            title: {
                describe: 'Update a newspost title',
                demandOption: true,
                type: 'string',
            },
            text: {
                describe: 'Update a newspost text',
                demandOption: true,
                type: 'string',
            },
        });
    })
    .help().argv;

const { id, title, text } = argv;

if (argv._[0] === 'update') {
    update(id, title, text);
} else {
    console.log("Invalid command. Please use: node script.js update --id=id --title='New title' --text='New text'");
}
