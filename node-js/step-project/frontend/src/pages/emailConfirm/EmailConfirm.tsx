import { useNavigate } from "react-router-dom"

function EmailConfirm() {
    const navigate = useNavigate()

  return (
    <div>Your email confirmed please <button onClick={() => navigate('/login')}>login</button></div>
  )
}

export default EmailConfirm