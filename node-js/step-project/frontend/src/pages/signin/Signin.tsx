import './signin.css';
import { Formik, Form, Field, ErrorMessage, FormikHelpers } from 'formik';
import * as Yup from 'yup';
import Button from '../../components/UI/button/Button';
import UsersService from '../../API/usersService';
import { useState } from 'react';
import Modal from '../../components/UI/modal/Modal';
import { useNavigate } from 'react-router-dom';


interface Users {
  id?: string;
  name: string
  email: string;
  password: string;
}

const SignupSchema = Yup.object().shape({
  name: Yup.string().required('Name is required'),
  email: Yup.string().email('Invalid email').required('Email is required'),
  password: Yup.string().required('Password is required'),
});

const initialValues = {
  name: '',
  email: '',
  password: '',
};

function Signin() {
  const [showModal, setShowModal] = useState(false);
  const navigate = useNavigate()

  const handleSubmit = async (values: Users, { setErrors }: FormikHelpers<Users>) => {
    try {
   
      const response = await UsersService.register(values.name, values.email, values.password, );
  
      if (response.status === 201) {
        setShowModal(true)
        localStorage.setItem('token', response.data.confirmationToken)
        console.log(response);
        console.log('User registered successfully.');
      } else {
        console.error('Unexpected response:', response);
      }
      
    } catch (error: any) {
      if (error.response.status === 409) {
        setErrors({ email: 'This email already exist!' });
      } else {
        console.error(error);
      }
    }
  };
  
  return (
    <>
      <div className="signin__container">
        <div className="signin-left">
        <svg viewBox="0 0 24 24" aria-hidden="true"><g><path d="M18.244 2.25h3.308l-7.227 8.26 8.502 11.24H16.17l-5.214-6.817L4.99 21.75H1.68l7.73-8.835L1.254 2.25H8.08l4.713 6.231zm-1.161 17.52h1.833L7.084 4.126H5.117z"></path></g></svg>
        </div>
        <div className="signin-right">
         <div className='signin-messages'>
          <span className='signin-message'>Happening now</span>
          <span className='signin-message'>Join today.</span>
         </div>

         <Formik
            initialValues={initialValues}
            validationSchema={SignupSchema}
            onSubmit={handleSubmit}
          >
              <Form>
                <Field type="name" name="name" placeholder="Name" />
                <ErrorMessage name="name" component="div" className="error-message"/>
                <Field type="email" name="email" placeholder="Email" />
                <ErrorMessage name="email" component="div" className="error-message"/>
                <Field type="password" name="password" placeholder="Password" />
                <ErrorMessage name="password" component="div" className="error-message"/>
                <div className="button__wrapper">
                  <Button variant="default" type="submit" size='large' >
                    Sign In
                  </Button>
                </div>
              </Form>
          </Formik>
          <span className='login'>
            Have an account?
            <Button variant="default" size='medium' onClick={() => navigate('/login')}>Log in</Button>
          </span>
         </div>
         
        </div>
        {showModal && (
        <Modal onClose={() => setShowModal(false)}>
          <p>Please confirm your email</p>
        </Modal>)}
    </>
  );
}

export default Signin;
