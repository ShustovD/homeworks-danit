import './login.css';
import { Formik, Form, Field, ErrorMessage, FormikHelpers } from 'formik';
import * as Yup from 'yup';
import Button from '../../components/UI/button/Button';
import UsersService from '../../API/usersService';
import { useNavigate } from 'react-router-dom';


interface Users {
  id?: string;
  email: string;
  password: string;
}

const SignupSchema = Yup.object().shape({
  email: Yup.string().email('Invalid email').required('Email is required'),
  password: Yup.string().required('Password is required'),
});

const initialValues = {
  name: '',
  email: '',
  password: '',
};

function Login() {
  const navigate = useNavigate()

  const handleSubmit = async (values: Users, { setErrors }: FormikHelpers<Users>) => {
    try {
   
      const response = await UsersService.login(values.email, values.password);
  
      if (response.status === 201) {
        navigate('/')
       console.log("login success");
      } else {
        console.error('Unexpected response:', response);
      }
      
    } catch (error: any) {
      if (error.response.status === 409) {
        setErrors({ email: 'This email already exist!' });
      } else {
        console.error(error);
      }
    }
  };
  
  return (
    <>
      <div className="login__container">
        <div className="login-left">
        <svg viewBox="0 0 24 24" aria-hidden="true"><g><path d="M18.244 2.25h3.308l-7.227 8.26 8.502 11.24H16.17l-5.214-6.817L4.99 21.75H1.68l7.73-8.835L1.254 2.25H8.08l4.713 6.231zm-1.161 17.52h1.833L7.084 4.126H5.117z"></path></g></svg>
        </div>
        <div className="login-right">
         <div className='login-messages'>
          <span className='login-message'>Happening now</span>
          <span className='login-message'>Join today.</span>
         </div>

         <Formik
            initialValues={initialValues}
            validationSchema={SignupSchema}
            onSubmit={handleSubmit}
          >
              <Form>
                <Field type="email" name="email" placeholder="Email" />
                <ErrorMessage name="email" component="div" className="error-message"/>
                <Field type="password" name="password" placeholder="Password" />
                <ErrorMessage name="password" component="div" className="error-message"/>
                <div className="button__wrapper">
                  <Button variant="default" type="submit" size='large' >
                    Log In
                  </Button>
                </div>
              </Form>
          </Formik>
          <span className='signin'>
            Do not have an account?
            <Button variant="default" size='medium' onClick={() => navigate('/signin')}>Sign in</Button>
          </span>
         </div>
         
        </div>
    </>
  );
}

export default Login;
