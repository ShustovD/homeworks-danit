import React, { useState, useRef, useEffect } from 'react';
import './NavSidebar.css';
import HomeIcon from '@mui/icons-material/Home';
import SearchIcon from '@mui/icons-material/Search';
import NotificationsNoneOutlinedIcon from '@mui/icons-material/NotificationsNoneOutlined';
import EmailOutlinedIcon from '@mui/icons-material/EmailOutlined';
import ListAltOutlinedIcon from '@mui/icons-material/ListAltOutlined';
import BookmarkBorderOutlinedIcon from '@mui/icons-material/BookmarkBorderOutlined';
import PeopleOutlinedIcon from '@mui/icons-material/PeopleOutlined';
import MenuIcon from '@mui/icons-material/Menu';
import PersonOutlineOutlinedIcon from '@mui/icons-material/PersonOutlineOutlined';
import { Avatar } from '@mui/material';
import { Link, useNavigate } from 'react-router-dom';
// import { useDispatch, useSelector } from 'react-redux';
// import { setLogout } from '../../redux/authSlice';
import axios from 'axios';

interface User {
  _id: string;
  username: string;
  // Add other properties as needed
}

function NavSidebar() {
  // const dispatch = useDispatch();
  const navigate = useNavigate();
  const ref = useRef<HTMLDivElement | null>(null);

  const [user, setUser] = useState<User | null>(null);
  const [showMore, setShowMore] = useState(false);


  const getUser = async () => {
    try {
      const response = await axios.get(`http://localhost:3001/users/}`, {
        // headers: { Authorization: `Bearer ${token}` },
      });
      setUser(response.data);
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    getUser();
  }, []);

  const style: React.CSSProperties = {
    width: '28px',
    height: '28px',
    fill: "white"
  };

  return (
    <div className="sidenav">

      <svg style={style} className='sidenav__logo' viewBox="0 0 24 24" aria-hidden="true"><g><path d="M18.244 2.25h3.308l-7.227 8.26 8.502 11.24H16.17l-5.214-6.817L4.99 21.75H1.68l7.73-8.835L1.254 2.25H8.08l4.713 6.231zm-1.161 17.52h1.833L7.084 4.126H5.117z"></path></g></svg>
   
      <div className="sidenav__buttons">
        <Link to="/">
          <button className="sidenav__button">
            <HomeIcon style={style} />
            <span>Home</span>
          </button>
        </Link>
        {/* Add other Link components as needed */}
        <button className="sidenav__button">
          <SearchIcon style={style} />
          <span>Explore</span>
        </button>
        <button className="sidenav__button">
          <NotificationsNoneOutlinedIcon style={style} />
          <span>Notification</span>
        </button>
        <button className="sidenav__button">
          <EmailOutlinedIcon style={style} />
          <span>Messages</span>
        </button>
        <button className="sidenav__button">
          <ListAltOutlinedIcon style={style} />
          <span>Lists</span>
        </button>
        <button className="sidenav__button">
          <BookmarkBorderOutlinedIcon style={style} />
          <span>Bookmarks</span>
        </button>
        <button className="sidenav__button">
          <PeopleOutlinedIcon style={style} />
          <span>Communities</span>
        </button>
        <button className="sidenav__button">
        <svg style={style} viewBox="0 0 24 24" aria-hidden="true"><g><path d="M18.244 2.25h3.308l-7.227 8.26 8.502 11.24H16.17l-5.214-6.817L4.99 21.75H1.68l7.73-8.835L1.254 2.25H8.08l4.713 6.231zm-1.161 17.52h1.833L7.084 4.126H5.117z"></path></g></svg>
          <span>Premium</span>
        </button>
        <button className="sidenav__button"   onClick={() => {
            // navigate(`/profile/${_id}`);
            navigate(0);
          }}>
          <PersonOutlineOutlinedIcon style={style} />
          <span>Profile</span>
        </button>
        <button className="sidenav__button">
        <MenuIcon style={style} />
          <span className="sidenav__buttonText">More</span>
          </button>
      
      </div>
      <div className="sidenav__more" ref={ref}>
        {showMore && (
          <div className="sidenav__dropdown">
            <button className="logout__dropdown-button">
              Logout
            </button>
          </div>
        )}
        <button
          className="sidenav__button logout"
          onClick={() => {
            setShowMore((v) => !v);
          }}
        >
          <Avatar />
          <span>{}</span>
          <MenuIcon style={style} />
        </button>
      </div>
    </div>
  );
}

export default NavSidebar;
