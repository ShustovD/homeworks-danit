import React from 'react';
import './Suggesstions.css';
import { Avatar } from '@mui/material';
import { useNavigate } from 'react-router-dom';
// import { useDispatch, useSelector } from 'react-redux';
import axios from 'axios';
// import { setFollowers } from '../../../redux/authSlice';

interface SuggestionsProps {
    friendId: string;
    username: string;
    createdAt: string;
}

const Suggestions: React.FC<SuggestionsProps> = ({ friendId, username, createdAt }) => {
    const navigate = useNavigate();
    // const dispatch = useDispatch();
    // const { _id } = useSelector((state: RootState) => state.user);
    // const token = useSelector((state: RootState) => state.token);

    const patchFriend = async () => {
        try {
            const response = await axios.patch(
                `http://localhost:3001/users/`,
                {},
                {
                    // headers: {
                    //     Authorization: `Bearer ${token}`,
                    //     'Content-Type': 'application/json',
                    // },
                }
            );

            const data = response.data;
            return data
        } catch (error) {
            console.error(error);
        }
    };

    return (
        <div className="suggestions">
            <div className="suggestions__usernames">
                <div className="suggestion__username">
                    <div className="username__left">
                        <button className="avatar" onClick={() => navigate(`profile/${friendId}`)}>
                            <Avatar>{username.charAt(0).toUpperCase()}</Avatar>
                        </button>
                        <div className="username__info">
                            <span className="username">{username}</span>
                            <span className="relation">{createdAt}</span>
                        </div>
                    </div>
                    <button className="follow__button" onClick={() => patchFriend()}>
                        Follow
                    </button>
                </div>
            </div>
        </div>
    );
};

export default Suggestions;
