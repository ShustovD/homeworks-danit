import React, { useEffect, useState } from 'react';
import './Timeline.css';
import Suggesstions from './suggestions/Suggesstions';
import Post from './posts/Post';
import Stories from './Stories';
import { Avatar } from '@mui/material';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import PostsService from '../../API/postsService';

function Timeline(): JSX.Element {
    const [posts, setPosts] = useState([]);

    const navigate = useNavigate();

    useEffect(() => {
        // Define an async function to fetch posts
        const fetchPosts = async () => {
            try {
                const response = await PostsService.getPosts();
                setPosts(response.data); // Assuming the posts are in the response data
            } catch (error) {
                console.error('Error fetching posts:', error);
            }
        };

        // Call the async function to fetch posts when the component mounts
        fetchPosts();
    }, []); // The empty dependency array ensures that this effect runs only once when the component mounts

    console.log( posts);
    
    return (
        <div className="timeline">
            <div className="timeline__left">
                <div className="timeline__posts">
                    <Stories />
                    {/* {posts.map((post, index) => (
                        <Post key={index} user={post.user} postImage={post.postImage} likes={post.likes} timestamp={post.timestamp} />
                    ))} */}
                </div>
            </div>
            <div className="timeline__right">
                <div className="suggesstions__top">
                    <Avatar
                        onClick={() => {
                            navigate(`/profile/`);
                            navigate(0);
                        }}
                    ></Avatar>
                    {/* <span>{user.username}</span> */}
                </div>
                <div className="suggesstions__title">Suggestions for you</div>
            </div>
        </div>
    );
}

export default Timeline;
