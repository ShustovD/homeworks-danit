import { Route, Routes } from 'react-router-dom';
import './App.css';
import Signin from './pages/signin/Signin';
import EmailConfirm from './pages/emailConfirm/EmailConfirm';
import Login from './pages/login/Login';
import Home from './pages/home/Home';


function App() {
  const token = localStorage.getItem('token')
  console.log(token);
  
  return (
      <div>
        <Routes>
        <Route path='/signin' element={<Signin/>}/>
        <Route path='/login' element={<Login/>}/>
        <Route path='/' element={<Home/>}/>
        <Route path={`/confirm/email?token=${token}`} element={<EmailConfirm/>}/>
        </Routes>
      </div>
  );
}

export default App;
