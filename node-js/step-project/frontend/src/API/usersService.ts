import axios from "axios"

export default class UsersService {
    static async register(name:string, email: string, password: string) {
        try {
            const response = await axios.post(`http://localhost:3000/users`, {
                name,
                email,
                password,
              }, 
              {
                withCredentials: true, 
              });
         
            return response
        } catch (error) {
            throw error
        }
    }
    static async login(email: string, password: string) {
        try {
            const response = await axios.post(`http://localhost:3000/auth/login`, {
                email,
                password,
              }, 
              {
                withCredentials: true, 
              });

            return response
        } catch (error) {
            throw error
        }
    }
    static async emailConfirm(token: string) {
      try {
          const response = await axios.get(`http://localhost:3000/confirm/email?token=${token}`)
          return response
      } catch (error) {
          throw error
      }
  }
}