import axios from "axios";

export default class PostsService {
    static async getPosts() {
        try {
            const response = await axios.get(`http://localhost:3000/articles`);
         
            return response
        } catch (error) {
            throw error
        }
    }
}