import { Module } from '@nestjs/common';
import { UsersService } from './users.service';
import { UsersController } from './users.controller';
import { PrismaModule } from 'src/prisma/prisma.module';
import { EmailService } from 'src/email/email.service';
import { JwtModule } from '@nestjs/jwt';

@Module({
  controllers: [UsersController],
  providers: [UsersService, EmailService],
  imports: [
    PrismaModule,  
    JwtModule.register({
    secret: process.env.JWT_SECRET,
    signOptions: { expiresIn: '5m' }, 
  }),],
  exports: [UsersService],
})
export class UsersModule {}
