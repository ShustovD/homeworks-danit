import { Controller, Get, NotFoundException, Query,  } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { EmailService } from './email.service';

@Controller('confirm')
@ApiTags('confirm')
export class EmailController {
  constructor(private readonly emailService: EmailService) {}

  @Get('email')
  async confirmEmail(@Query('token') token: string): Promise<void> {
    // Verify the token and update the user's email confirmation status
    const user = await this.emailService.confirmEmail(token);

    if (!user) {
      throw new NotFoundException('Invalid or expired confirmation token.');
    }

    // Additional logic (e.g., set user as confirmed, update database, etc.)
  }
}

