import { Injectable, NotFoundException,  } from '@nestjs/common';
import { MailerService } from '@nestjs-modules/mailer';
import { PrismaService } from 'src/prisma/prisma.service';

@Injectable()
export class EmailService {
  constructor(
    private readonly mailerService: MailerService,
    private prisma: PrismaService,
    ) {}

  async sendConfirmationEmail(email: string, token: string): Promise<void> {
    const url = `http://localhost:3001/confirm/email?token=${token}`;

    await this.mailerService.sendMail({
      to: email,
      from:'noreply@nestjs.com',
      subject: 'Confirm your email',
      html: `
        <h3>Hello!!!</h3>
        <p>Please click this <a href="${url}">link</a> to confirm your email.</p>
      `
    });
  }

  async confirmEmail(token: string): Promise<any> {
    // Verify the token and update the user's email confirmation status
    // Example: You may want to use your JWTService to verify the token

    // For simplicity, this example assumes a direct database update
    const user = await this.prisma.user.findUnique({
      where: { confirmationToken: token },
    });

    if (!user) {
      throw new NotFoundException('Invalid or expired confirmation token.');
    }

    // Update user's email confirmation status (example: set confirmed to true)
    const updatedUser = await this.prisma.user.update({
      where: { id: user.id },
      data: { isEmailConfirmed: true, confirmationToken: null },
    });

    return updatedUser;
  }
  
}
