import { Body, Controller, Get, Post, Req, UseGuards } from '@nestjs/common';
import { AuthService } from './auth.service';
import { ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { AuthEntity } from './entity/auth.entity';
import { LoginDto } from './dto/login.dto';
import { AuthGuard } from '@nestjs/passport';

@Controller('auth')
@ApiTags('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Get('google')
  @UseGuards(AuthGuard('google'))
  @ApiOkResponse({type: AuthEntity})
  async googleLogin(@Body() { email }) {
    return this.authService.findOrCreate(email)
  }

  @Get('google/callback')
  @UseGuards(AuthGuard('google'))
  @ApiOkResponse({type: AuthEntity})
  async googleLoginCallback(@Req() req) {
    return {
      message: 'User information from Google',
      user: req.user,
    };
  }

  @Post('login')
  @ApiOkResponse({type: AuthEntity})
  login(@Body() { email, password }: LoginDto) {
    return this.authService.login(email, password)
  }


}
