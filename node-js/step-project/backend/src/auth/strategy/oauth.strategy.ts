import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { AuthService } from '../auth.service';
import { Strategy as OAuth2Strategy }from 'passport-oauth2';

@Injectable()
export class OauthStrategy extends PassportStrategy(OAuth2Strategy, 'google') {
  constructor(private readonly authService: AuthService) {
    super({
      authorizationURL: 'https://accounts.google.com/o/oauth2/auth',
      tokenURL: 'https://accounts.google.com/o/oauth2/token',
      clientID: 'YOUR_GOOGLE_CLIENT_ID',
      clientSecret: 'YOUR_GOOGLE_CLIENT_SECRET',
      callbackURL: 'http://localhost:3000/auth/google/callback',
    });
  }

  async validate(accessToken: string, refreshToken: string, profile: any) {
    // Add your logic to save or retrieve the user from the database
    const user = await this.authService.findOrCreate(profile);
    return user;
  }
}
