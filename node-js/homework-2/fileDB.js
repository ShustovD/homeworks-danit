import fs from 'fs';

const fileDB = {
    schemas: {},
    registerSchema(schemaName, schema) {
        this.schemas[schemaName] = schema;
    },
    async getTable(schemaName) {
        const schema = this.schemas[schemaName];
        if (!schema) {
            console.log(`Schema: '${schemaName}' is not registered!`);
            return null;
        }
        const table = {
            data: [],
            nextId: 1,
            async getAll() {
                try {
                    const rawData = await fs.promises.readFile(`${schemaName}.json`, 'utf8');
                    this.data = JSON.parse(rawData);
                    return this.data;
                } catch (error) {
                    console.log(error);
                }
                return null;
            },
            async getById(id) {
                try {
                    return this.data.find((item) => item.id === id);
                } catch (error) {
                    console.log(error);
                }
                return null;
            },
            async create(newsPost) {
                try {
                    const validatedItem = {};
                    Object.keys(schema).forEach((key) => {
                        validatedItem[key] = newsPost[key];
                    });
                    validatedItem.id = this.nextId;
                    this.nextId += 1;
                    validatedItem.createDate = new Date();
                    const rawData = await fs.promises.readFile(`${schemaName}.json`, 'utf8');
                    this.data = JSON.parse(rawData);
                    this.data.push(validatedItem);
                    this.saveToFile();
                    return validatedItem;
                } catch (error) {
                    console.log(error);
                }
                return null;
            },
            async update(id, updatedFields) {
                try {
                    const rawData = await fs.promises.readFile(`${schemaName}.json`, 'utf8');
                    this.data = JSON.parse(rawData);
                    const index = this.data.findIndex((item) => item.id === id);
                    const allowedFields = ['title', 'text'];
                    const updatedItem = { ...this.data[index] };

                    Object.keys(updatedFields).forEach((field) => {
                        if (allowedFields.includes(field)) {
                            updatedItem[field] = updatedFields[field];
                        }
                    });

                    this.data[index] = updatedItem;
                    this.saveToFile();
                    return updatedItem;
                } catch (error) {
                    console.log(error);
                }
                return null;
            },
            async delete(id) {
                try {
                    const rawData = await fs.promises.readFile(`${schemaName}.json`, 'utf8');
                    this.data = JSON.parse(rawData);
                    const index = this.data.findIndex((item) => item.id === id);
                    const deletedId = this.data[index].id;
                    this.data.splice(index, 1);
                    this.saveToFile();
                    return deletedId;
                } catch (error) {
                    console.log(error);
                }
                return null;
            },
            async saveToFile() {
                try {
                    await fs.promises.writeFile(`${schemaName}.json`, JSON.stringify(this.data, null, 2));
                } catch (error) {
                    console.log(error);
                }
            },
        };
        try {
            const rawData = await fs.promises.readFile(`${schemaName}.json`, 'utf8');
            table.data = JSON.parse(rawData);
            if (table.data.length > 0) {
                table.nextId = Math.max(...table.data.map((item) => item.id)) + 1;
            }
        } catch (error) {
            console.log(error);
        }

        return table;
    },
};

export default fileDB;
