import fileDB from './models/newsPosts.js';

async function test() {
    const newspostTable = await fileDB.getTable('newspost');
    // повертаємо усі записи у базі у вигляді масиву
    const newsposts = await newspostTable.getAll();
    console.log('Всі пости:', newsposts);

    // повертаємо запис за вказаним id
    const newspost = await newspostTable.getById(1);
    console.log('Пост по id:', newspost);

    // додаємо новий запис, та повертаємо його з новим id
    const data = {
        title: 'У зоопарку Чернігова лисичка народила лисеня',
        text: "В Чернігівському заопарку сталася чудова подія! Лисичка на ім'я Руда народила чудове лисенятко! Тож поспішайте навідатись та подивитись на це миле створіння!",
    };
    const createdNewspost = await newspostTable.create(data);
    console.log('Пост створено:', createdNewspost);

    // оновлюємо поле title за вказаним id та повертаємо оновлений запис
    const updatedNewsposts = await newspostTable.update(1, {
        title: 'Велика лисичкsdіssssііііsdа',
        sds: 'sds',
    });
    console.log('Пост було оновлено:', updatedNewsposts);

    // видаляємо записа за вказаним id та повертаємо id видаленого запису
    const deletedId = await newspostTable.delete(1);
    console.log(`Пост з id ${deletedId} було видалено`);
}

test();
