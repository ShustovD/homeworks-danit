import fileDB from '../fileDB.js';

const newspostSchema = {
    id: Number,
    title: String,
    text: String,
    createDate: Date,
};

fileDB.registerSchema('newspost', newspostSchema);

export default fileDB;
