const films = document.querySelector('films');
fetch('https://ajax.test-danit.com/api/swapi/films')
    .then(response => response.json())
    .then(films => {
        const filmsList = document.getElementById('films');
        films.forEach(film => {
            const filmItem = document.createElement('li');
            filmItem.innerHTML = `<b>Episode ${film.episodeId}: ${film.name}</b><br>${film.openingCrawl}<br><button>Show characters</button><ul></ul>`;
            filmsList.appendChild(filmItem);

            const showCharactersBtn = filmItem.querySelector('button');
            const charactersList = filmItem.querySelector('ul');
            showCharactersBtn.addEventListener('click', async () => {
                if (charactersList.childNodes.length > 0) {
                    charactersList.innerHTML = '';
                } else {
                    for (const characterUrl of film.characters) {
                        try {
                            const response = await fetch(characterUrl);
                            const character = await response.json();
                            const characterItem = document.createElement('li');
                            characterItem.innerHTML = `<b>Name: </b>${character.name}`;
                            charactersList.appendChild(characterItem);
                        } catch (error) {
                            console.error(error);
                        }
                    }
                }
            });
        });
    })
    .catch(error => {
        console.error(error);
    });
