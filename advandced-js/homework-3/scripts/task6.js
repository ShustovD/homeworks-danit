const employee = {
    name: 'Vitalii',
    surname: 'Klichko',
};

const newEmployee = {
    ...employee,
    age: 51,
    salary: 50000,
};

console.log(newEmployee);
