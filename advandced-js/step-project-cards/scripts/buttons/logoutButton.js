const loginWrapper = document.querySelector('.header__login');
const openCreateVisit = document.getElementById('open-create-visit');
const openLogin = document.getElementById('open-login');

const createLogoutButton = () => {
    const token = localStorage.getItem('token');
    const logoutButton = document.createElement('button');
    logoutButton.innerText = 'Log out';
    logoutButton.className = 'header__logout-button';
    logoutButton.addEventListener('click', () => {
        localStorage.removeItem('token');
        localStorage.removeItem('logoutState');
        openCreateVisit.style.display = 'none';
        loginWrapper.removeChild(logoutButton);
        openLogin.style.display = 'block';
        clearVisits(); // Очищення відображених візитів при виході з авторизації
    });

    if (token) {
        const logoutState = localStorage.getItem('logoutState');
        if (logoutState) {
            logoutButton.style.display = logoutState === 'visible' ? 'block' : 'none';
        }
        loginWrapper.appendChild(logoutButton);
    }

    return logoutButton;
};

export const updateLoginStatus = () => {
    const token = localStorage.getItem('token');
    openCreateVisit.style.display = token ? 'block' : 'none';
    openLogin.style.display = token ? 'none' : 'block';
    if (token) {
        const logoutButton = createLogoutButton();
        const logoutState = logoutButton.style.display === 'none' ? 'hidden' : 'visible';
        localStorage.setItem('logoutState', logoutState);
    } else {
        localStorage.removeItem('logoutState');
    }
};

window.addEventListener('load', updateLoginStatus);

function clearVisits() {
    const cardsSection = document.getElementById('cards-section');
    cardsSection.innerHTML = '';
}
