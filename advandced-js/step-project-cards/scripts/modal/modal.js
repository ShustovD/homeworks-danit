class Modal {
    constructor() {
        this.modalElement = document.querySelector('.modal');
        this.modalContentElement = this.modalElement.querySelector('.modal-content');
        this.modalElement.addEventListener('click', this.handleModalClick.bind(this));
        window.addEventListener('click', this.handleWindowClick.bind(this));
    }

    open(targetContent) {
        this.modalContentElement.innerHTML = '';
        this.modalContentElement.insertAdjacentHTML('beforeend', targetContent);
        this.modalElement.style.display = 'block';
        setTimeout(() => (this.modalElement.style.opacity = '1'), 0);
    }

    close() {
        this.modalElement.style.opacity = '0';
        setTimeout(() => (this.modalElement.style.display = 'none'), 300);
    }

    handleModalClick(event) {
        if (event.target.classList.contains('close-button')) {
            this.close();
        }
    }

    handleWindowClick(event) {
        if (event.target === this.modalElement) {
            this.close();
        }
    }
}

export default Modal;
