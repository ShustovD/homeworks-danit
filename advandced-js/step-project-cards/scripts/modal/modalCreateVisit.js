import Modal from './modal.js';

import { generateDoctorFields } from '../functions/generateDoctor.js';
import { createVisit } from '../fetch/createVisit.js';

const openVisit = document.getElementById('open-create-visit');
const modal = new Modal();

openVisit.addEventListener('click', () => {
    const targetContent = `
        <span class="close-button">&times;</span>
        <h2>Запись к врачу</h2>
        <label for="doctor">Выберите врача:</label>
        <select id="doctor">
            <option value="none">Выберите...</option>
            <option value="cardiologist">Кардиолог</option>
            <option value="dentist">Стоматолог</option>
            <option value="therapist">Терапевт</option>
        </select>
        <div id="fields-container"></div>
        <button id="create-button" class="create-button">Создать</button>
    `;

    modal.open(targetContent);

    const doctorSelect = document.getElementById('doctor');
    const fieldsContainer = document.getElementById('fields-container');

    doctorSelect.addEventListener('change', () => {
        const selectedDoctor = doctorSelect.value;

        fieldsContainer.innerHTML = generateDoctorFields(selectedDoctor);
    });

    const createButton = document.getElementById('create-button');
    createButton.addEventListener('click', () => {
        createVisit();
        modal.close();
    });
});
