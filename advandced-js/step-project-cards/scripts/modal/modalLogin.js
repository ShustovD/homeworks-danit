import Modal from './modal.js';
import { updateLoginStatus } from '../buttons/logoutButton.js';
import { renderCard } from '../functions/renderCard.js';
import { getAllVisits } from '../fetch/getAllVisits.js';

const modalContent = document.getElementById('modal-content');
const openLogin = document.getElementById('open-login');
const modal = new Modal();

openLogin.addEventListener('click', () => {
    const targetContent = `
    <span class="close-button">&times;</span>
      <h2>Вхід</h2>
      <form id="form" class="form">
        <label for="email">Email:</label>
        <input type="email" name="email" id="email">
        <label for="password">Password:</label>
        <input type="password" id="password" name="password">
        <button id="submit" type="submit">Увійти</button>
      </form>
  `;
    modal.open(targetContent);
});

modalContent.addEventListener('submit', async e => {
    e.preventDefault();
    const response = await fetch('https://ajax.test-danit.com/api/v2/cards/login', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            email: email.value,
            password: password.value,
        }),
    });
    if (!response.ok) {
        alert('Incorrect email or password');
        return;
    }
    const token = await response.text();
    localStorage.setItem('token', token);
    modal.close();
    updateLoginStatus();
    getAllVisits();
});

window.addEventListener('DOMContentLoaded', () => {
    const token = localStorage.getItem('token');
    if (token) {
        getAllVisits();
    }
});

export async function getAndRenderVisits() {
    const visits = await getAllVisits();
    const cardsContainer = document.getElementById('cards-container');

    cardsContainer.innerHTML = '';

    if (visits.length === 0) {
        const noItemsMessage = document.querySelector('.no-items-message');
        noItemsMessage.style.display = 'block';
    } else {
        visits.forEach(visit => {
            renderCard(visit);
        });
    }
}

// export async function deleteVisitAndUpdateCard(cardId) {
//     await deleteVisit(cardId);
//     const cardElement = document.getElementById(`card-${cardId}`);
//     cardElement.remove();
// }

// export async function updateVisitAndCloseModal(cardId, updatedData) {
//     await updateVisit(cardId, updatedData);
//     modal.close();
// }
