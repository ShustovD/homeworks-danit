export function generateDoctorFields(selectedDoctor) {
    let doctorFields = '';

    if (selectedDoctor === 'cardiologist') {
        doctorFields = `
          <label for="purpose">Цель визита:</label>
          <input type="text" id="purpose"><br>
          <label for="description">Краткое описание визита:</label>
          <input type="text" id="description"><br>
          <label for="urgency">Срочность:</label>
          <select id="urgency">
              <option value="Обычная">Обычная</option>
              <option value="Приоритетная">Приоритетная</option>
              <option value="Неотложная">Неотложная</option>
          </select><br>
          <label for="fullName">ФИО:</label>
          <input type="text" id="fullName"><br>
          <label for="pressure">Обычное давление:</label>
          <input type="text" id="pressure"><br>
          <label for="bmi">Индекс массы тела:</label>
          <input type="text" id="bmi"><br>
          <label for="heartDiseases">Перенесенные заболевания сердечно-сосудистой системы:</label>
          <input type="text" id="heartDiseases"><br>
          <label for="age">Возраст:</label>
          <input type="text" id="age"><br>
      `;
    } else if (selectedDoctor === 'dentist') {
        doctorFields = `
          <label for="purpose">Цель визита:</label>
          <input type="text" id="purpose"><br>
          <label for="description">Краткое описание визита:</label>
          <input type="text" id="description"><br>
          <label for="urgency">Срочность:</label>
          <select id="urgency">
              <option value="Обычная">Обычная</option>
              <option value="Приоритетная">Приоритетная</option>
              <option value="Неотложная">Неотложная</option>
          </select><br>
          <label for="fullName">ФИО:</label>
          <input type="text" id="fullName"><br>
          <label for="lastVisitDate">Дата последнего посещения:</label>
          <input type="date" id="lastVisitDate"><br>
      `;
    } else if (selectedDoctor === 'therapist') {
        doctorFields = `
          <label for="purpose">Цель визита:</label>
          <input type="text" id="purpose"><br>
          <label for="description">Краткое описание визита:</label>
          <input type="text" id="description"><br>
          <label for="urgency">Срочность:</label>
          <select id="urgency">
              <option value="Обычная">Обычная</option>
              <option value="Приоритетная">Приоритетная</option>
              <option value="Неотложная">Неотложная</option>
          </select><br>
          <label for="fullName">ФИО:</label>
          <input type="text" id="fullName"><br>
          <label for="age">Возраст:</label>
          <input type="text" id="age"><br>
      `;
    }
    return doctorFields;
}
