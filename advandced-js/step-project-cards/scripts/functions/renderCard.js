import Modal from '../modal/modal.js';
import { deleteVisit } from '../fetch/deleteVisit.js';
import { generateDoctorFields } from './generateDoctor.js';

const modal = new Modal();
function showModal(cardData) {
    const modalContent = document.createElement('div');

    const selectedDoctor = cardData.doctor;
    const doctorField = generateDoctorFields(selectedDoctor);
    modalContent.innerHTML = doctorField;
    modal.open(modalContent);

    console.log(selectedDoctor);
}
export function renderCard(cardData) {
    const cardsSection = document.getElementById('cards-section');

    const cardElement = document.createElement('div');
    cardElement.classList.add('card');
    cardElement.id = `card-${cardData.id}`;

    const cardContent = document.createElement('div');
    cardContent.classList.add('card-content');

    const doctorName = document.createElement('h3');
    doctorName.textContent = `Doctor: ${cardData.doctor}`;
    cardContent.appendChild(doctorName);

    const fullName = document.createElement('p');
    fullName.textContent = `Full name: ${cardData.fullName}`;
    cardContent.appendChild(fullName);

    // Create Show more button
    const showMoreButton = document.createElement('button');
    showMoreButton.textContent = 'Show More';
    cardContent.appendChild(showMoreButton);

    // Create Edit button
    const editButton = document.createElement('button');
    editButton.textContent = 'Edit';
    cardContent.appendChild(editButton);
    editButton.addEventListener('click', () => {
        showModal(cardData);
    });

    // Create Delete button
    const deleteButton = document.createElement('button');
    deleteButton.textContent = 'Delete';
    cardContent.appendChild(deleteButton);
    deleteButton.addEventListener('click', () => {
        deleteVisit(cardData.id);
    });

    const additionalFields = {
        Кардіолог: [
            { label: 'Purpose', value: cardData.purpose },
            { label: 'Description', value: cardData.description },
            { label: 'Urgency', value: cardData.urgency },
            { label: 'Pressure', value: cardData.pressure },
            { label: 'BMI', value: cardData.bmi },
            { label: 'Heart Diseases', value: cardData.heartDiseases },
            { label: 'Age', value: cardData.age },
        ],
        Стоматолог: [
            { label: 'Purpose', value: cardData.purpose },
            { label: 'Description', value: cardData.description },
            { label: 'Urgency', value: cardData.urgency },
            { label: 'Last Visit Date', value: cardData.lastVisitDate },
        ],
        Терапевт: [
            { label: 'Purpose', value: cardData.purpose },
            { label: 'Description', value: cardData.description },
            { label: 'Urgency', value: cardData.urgency },
            { label: 'Age', value: cardData.age },
        ],
    };

    function showMore() {
        if (cardContent.querySelectorAll('.additional-field').length === 0) {
            showMoreButton.textContent = 'Hide';

            additionalFields[cardData.doctor].forEach(field => {
                const fieldElement = document.createElement('p');
                fieldElement.textContent = `${field.label}: ${field.value}`;
                fieldElement.classList.add('additional-field');
                cardContent.insertBefore(fieldElement, showMoreButton);
            });
        } else {
            showMoreButton.textContent = 'Show More';
            const additionalDataElements = cardContent.querySelectorAll('.additional-field');
            additionalDataElements.forEach(element => {
                cardContent.removeChild(element);
            });
        }
    }
    showMoreButton.addEventListener('click', showMore);

    cardElement.appendChild(cardContent);
    cardsSection.appendChild(cardElement);
}
