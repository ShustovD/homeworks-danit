import Visit from './Visit.js';

export class VisitCardiologist extends Visit {
    constructor(id, doctor, purpose, description, urgency, fullName, age, pressure, bmi, heartDiseases) {
        super(id, doctor, purpose, description, urgency, fullName);
        this.age = age;
        this.pressure = pressure;
        this.bmi = bmi;
        this.heartDiseases = heartDiseases;
    }

    getData() {
        return {
            doctor: 'Кардіолог',
            purpose: document.getElementById('purpose').value,
            description: document.getElementById('description').value,
            urgency: document.getElementById('urgency').value,
            fullName: document.getElementById('fullName').value,
            pressure: document.getElementById('pressure').value,
            bmi: document.getElementById('bmi').value,
            heartDiseases: document.getElementById('heartDiseases').value,
            age: document.getElementById('age').value,
        };
    }
}

export default VisitCardiologist;
