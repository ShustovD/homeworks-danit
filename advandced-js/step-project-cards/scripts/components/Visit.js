export class Visit {
    constructor(id, doctor, purpose, description, urgency, fullName) {
        this.id = id;
        this.doctor = doctor;
        this.purpose = purpose;
        this.description = description;
        this.urgency = urgency;
        this.fullName = fullName;
    }

    getData() {
        return {
            id: this.id,
            doctor: this.doctor,
            purpose: this.purpose,
            description: this.description,
            urgency: this.urgency,
            fullName: this.fullName,
        };
    }
}

export default Visit;
