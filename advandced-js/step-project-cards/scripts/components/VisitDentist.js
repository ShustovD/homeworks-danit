import Visit from './Visit.js';

export class VisitDentist extends Visit {
    constructor(id, doctor, purpose, description, urgency, fullName, lastVisitDate) {
        super(id, doctor, purpose, description, urgency, fullName);
        this.lastVisitDate = lastVisitDate;
    }

    getData() {
        return {
            id: this.id,
            doctor: 'Стоматолог',
            purpose: document.getElementById('purpose').value,
            description: document.getElementById('description').value,
            urgency: document.getElementById('urgency').value,
            fullName: document.getElementById('fullName').value,
            lastVisitDate: document.getElementById('lastVisitDate').value,
        };
    }
}

export default VisitDentist;
