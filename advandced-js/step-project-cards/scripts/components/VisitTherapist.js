import Visit from './Visit.js';
export class VisitTherapist extends Visit {
    constructor(id, doctor, purpose, description, urgency, fullName, age) {
        super(id, doctor, purpose, description, urgency, fullName);
        this.age = age;
    }

    getData() {
        return {
            id: this.id,
            doctor: 'Терапевт',
            purpose: document.getElementById('purpose').value,
            description: document.getElementById('description').value,
            urgency: document.getElementById('urgency').value,
            fullName: document.getElementById('fullName').value,
            age: document.getElementById('age').value,
        };
    }
}
export default VisitTherapist;
