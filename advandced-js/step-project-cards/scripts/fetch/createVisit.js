import VisitCardiologist from '../components/VisitCardiologist.js';
import VisitDentist from '../components/VisitDentist.js';
import VisitTherapist from '../components/VisitTherapist.js';
import { getVisit } from './getVisit.js';

export function createVisit() {
    const doctorSelect = document.getElementById('doctor');
    const selectedDoctor = doctorSelect.value;

    let visit;
    switch (selectedDoctor) {
        case 'cardiologist':
            visit = new VisitCardiologist();
            break;
        case 'dentist':
            visit = new VisitDentist();
            break;
        case 'therapist':
            visit = new VisitTherapist();
            break;
        default:
            alert('Ви не обрали лікаря!');
            return;
    }

    const visitData = visit.getData();

    sendVisitData(visitData);
}

function sendVisitData(data) {
    const token = localStorage.getItem('token');

    fetch('https://ajax.test-danit.com/api/v2/cards', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        },
        body: JSON.stringify(data),
    })
        .then(response => response.json())
        .then(response => {
            const cardId = response.id;
            getVisit(cardId);
        });
}
