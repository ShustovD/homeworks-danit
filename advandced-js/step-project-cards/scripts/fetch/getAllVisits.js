import { renderCard } from '../functions/renderCard.js';

export async function getAllVisits() {
    const token = localStorage.getItem('token');
    const response = await fetch('https://ajax.test-danit.com/api/v2/cards', {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        },
    });
    if (response.ok) {
        const cards = await response.json();
        renderAllCards(cards);
    } else {
        console.log('Error fetching visits');
    }
    function renderAllCards(cards) {
        const cardsSection = document.getElementById('cards-section');
        cardsSection.innerHTML = '';
        cards.forEach(cardData => {
            renderCard(cardData);
        });
    }
}
