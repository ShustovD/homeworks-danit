export async function deleteVisit(cardId) {
    const token = localStorage.getItem('token');
    const response = await fetch(`https://ajax.test-danit.com/api/v2/cards/${cardId}`, {
        method: 'DELETE',
        headers: {
            Authorization: `Bearer ${token}`,
        },
    });

    if (response.ok) {
        const cardElement = document.getElementById(`card-${cardId}`);
        if (cardElement) {
            cardElement.remove();
        }
    } else {
        console.error('Не удалось удалить карту:', response.status);
    }
}
