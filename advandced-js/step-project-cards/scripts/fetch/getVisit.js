import { renderCard } from '../functions/renderCard.js';

export async function getVisit(cardId) {
    const token = localStorage.getItem('token');
    const response = await fetch(`https://ajax.test-danit.com/api/v2/cards/${cardId}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        },
    });

    const cardData = await response.json();
    renderCard(cardData);
    console.log(cardData);
}
