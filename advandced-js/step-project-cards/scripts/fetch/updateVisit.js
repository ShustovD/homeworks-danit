export async function updateVisit(cardId, updatedData) {
    const token = localStorage.getItem('token');
    const response = await fetch(`https://ajax.test-danit.com/api/v2/cards/${cardId}`, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        },
        body: JSON.stringify(updatedData),
    });
    const responseData = await response.json();
    console.log('Card updated:', responseData);
}
