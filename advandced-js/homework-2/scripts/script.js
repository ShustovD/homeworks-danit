import { books } from './books.js';

const div = document.querySelector('#root');
const keysToCheck = ['author', 'name', 'price'];

books.forEach(book => {
    try {
        const missingKeys = keysToCheck.filter(key => !book.hasOwnProperty(key));
        if (missingKeys.length) {
            throw new Error(`Книга "${book.name}" має відсутні ключі: ${missingKeys}`);
        }
        const ul = document.createElement('ul');
        for (const [key, value] of Object.entries(book)) {
            const li = document.createElement('li');
            li.innerText = `${key}: ${value}`;
            ul.appendChild(li);
        }
        div.appendChild(ul);
    } catch (error) {
        console.error(error);
    }
});
