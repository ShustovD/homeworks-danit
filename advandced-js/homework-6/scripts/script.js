const findIp = document.querySelector('.ip-button');
const ipResult = document.querySelector('.ip-result');
async function findIP() {
    try {
        const ipResponse = await fetch('https://api.ipify.org/?format=json');
        const ipData = await ipResponse.json();
        const geoResponse = await fetch(`http://ip-api.com/json/${ipData.ip}`);
        const geoData = await geoResponse.json();

        ipResult.innerHTML = `
        <p>Континент: ${geoData.continent}</p>
        <p>Країна: ${geoData.country}</p>
        <p>Регіон: ${geoData.regionName}</p>
        <p>Місто: ${geoData.city}</p>
        <p>Район: ${geoData.district}</p>
        `;
        console.log(geoData);
    } catch (error) {
        console.error(error);
    }
}

findIp.addEventListener('click', findIP);
