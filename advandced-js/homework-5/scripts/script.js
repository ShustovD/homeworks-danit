const cardsContainer = document.getElementById('cards-container');

class Card {
    constructor(id, title, text, user) {
        this.id = id;
        this.title = title;
        this.text = text;
        this.user = user;
        this.cardElement = this.createCardElement();
        this.deleteButton = this.cardElement.querySelector('.delete-button');
        this.deleteButton.addEventListener('click', this.handleDelete.bind(this));
    }

    createCardElement() {
        const card = document.createElement('div');
        card.classList.add('card');
        card.innerHTML = `
            <h2 class="card-title">${this.title}</h2>
            <p class="card-text">${this.text}</p>
            <p class="card-user">${this.user.name} ${this.user.surname} (${this.user.email})</p>
            <button class="delete-button">Delete</button>
            `;
        return card;
    }

    handleDelete() {
        fetch(`https://ajax.test-danit.com/api/json/posts/${this.id}`, {
            method: 'DELETE',
        })
            .then(response => {
                response.json();
                this.cardElement.remove();
            })
            .catch(error => console.error(error));
    }
}

fetch('https://ajax.test-danit.com/api/json/users')
    .then(response => response.json())
    .then(users => {
        fetch('https://ajax.test-danit.com/api/json/posts')
            .then(response => response.json())
            .then(posts => {
                posts.forEach(post => {
                    const user = users.find(user => user.id);
                    const card = new Card(post.id, post.title, post.body, user);
                    cardsContainer.appendChild(card.cardElement);
                });
            })
            .catch(error => console.error(error));
    })
    .catch(error => console.error(error));
