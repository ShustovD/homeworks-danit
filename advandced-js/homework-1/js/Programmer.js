import { Employee } from './Employee.js';

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }

    get salary() {
        return super.salary * 3;
    }

    set salary(newSalary) {
        return (super.salary = newSalary);
    }
}

const newProgrammer = new Programmer('Lisa', 20, 400, 'English, Ukrainian');
const oldProgrammer = new Programmer('Alex', 30, 1000, 'English, Spanish, Ukrainian');

console.log(newProgrammer, oldProgrammer);
