// 1. Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript.
//     Наприклад, у нас э об'єкт зі своїми методами та власитвостями, але ми хочемо створити новий об'єкт з тими самими
//     методами та властивостями і щоб їх не копіювати ми використовуємо прототипне наслідування.
// 2. Для чого потрібно викликати super() у конструкторі класу-нащадка?
//    Для доступу до властивостей класу-нащадка.

export class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name() {
        return this._name;
    }

    set name(newName) {
        return (this._name = newName);
    }

    get age() {
        return this._age;
    }

    set age(newAge) {
        return (this._age = newAge);
    }

    get salary() {
        return this._salary;
    }

    set salary(newSalary) {
        return (this._salary = newSalary);
    }
}
