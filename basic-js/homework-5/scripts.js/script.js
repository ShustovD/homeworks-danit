// Теоретичні питання:
// 1. Опишіть своїми словами, що таке метод об'єкту?
// Це фунцкія, яка виступає у вигляді властивості об'єкту.
// 2. Який тип даних може мати значення властивості об'єкта?
// Значення властивості об'єкту може бути будь-яким типом даних.
// 3. Об'єкт це посилальний тип даних. Що означає це поняття?
// При використанні посилального типу даних копіюється його посилання, а не ціле значення.

// Практичне завдання:

function createNewUser() {
    const firstName = prompt('What is your name?');
    const lastName = prompt('What is your last name?');
    const newUser = {
        firstName,
        lastName,
        getLogin: function () {
            return (this.firstName[0] + this.lastName).toLowerCase();
        },
    };
    return newUser;
}
const user = createNewUser();
user.getLogin();
console.log(user);
console.log(user.getLogin());
