// Теоретичні питання:
// 1. Опишіть своїми словами як працює метод forEach.
// Метод forEach виконує вказану функцію для кожного елементу з масиву один раз.
// 2. Як очистити масив?
// Очистити масив можна за допомогою властивості length, якщо оголосити array.length = 0, то масив очиститься.
// 3. Як можна перевірити, що та чи інша змінна є масивом?
// Методом Array.isArray("назва змінної"), якщо змінна являється масивом поверне - true, і false якщо він їм не являється.

// Практичне завдання:

let array = [18, 'String', null, undefined, '25', 222, 12312];
let dataType = 'number';
let filterBy = (array, dataType) => array.filter(array => typeof array !== dataType);
console.log(filterBy(array, dataType));
