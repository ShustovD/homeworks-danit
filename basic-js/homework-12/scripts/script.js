// Теоретичні питання:
// 1. Чому для роботи з input не рекомендується використовувати клавіатуру?
// Тому що, користувач може вводити дані не тільки за допомогою клавіатури, він може:
// 1. Копіювати текст за допомогою миші.
// 2. Використовувати голосове веддення даних.
// Також не завжди рекомендується використовувати клавіатуру, тому що, event.key дозволяє отримати символ клавіатури навіть коли мова буде інша.
// Наприклад, якщо користувач веде букву "K", то при перемиканні на іншу мову символ буде інший. Тоді як event.code зберігає сам
// "фізичний код клавіши" і значення "K" так і залишиться навіть при змінні мови.

// Практичне завдання:
alert('Будь ласка, увімкніть англійську мову клавіатури! :)');

const buttonWrapper = document.querySelector('.btn-wrapper');
const wrapperChildrens = buttonWrapper.children;

function findKey(findKeyValue) {
    return [...wrapperChildrens].find(elem => elem.innerHTML.toUpperCase() === findKeyValue);
}

function removeActiveKey() {
    [...wrapperChildrens].forEach(e => {
        e.classList.remove('active');
    });
}

document.addEventListener('keydown', function (event) {
    const key = findKey(event.key.toUpperCase());
    removeActiveKey();
    if (!!key) {
        key.classList.add('active');
    }
});
