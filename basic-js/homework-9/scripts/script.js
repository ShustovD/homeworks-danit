// ## Теоретичні питання

// 1. Опишіть, як можна створити новий HTML тег на сторінці.
// HTML тег можна створити за допомогою document.createElement.
// 2. Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.
// Він визначає позицію елемента, є чотири варіації цього елемента 'beforebegin': до самого element (до відкриваючого тегу).
// 'afterbegin': відразу після тега element, що відкриває (перед першим нащадком).
// 'beforeend': відразу перед закриваючим тегом element (після останнього нащадку).
// 'afterend': після element (після закриваючого тега).
// 3. Як можна видалити елемент зі сторінки?
// За допомогою  методу element.remove().

// Практичне завдання:

const array = ['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv'];
const parent = document.querySelector('.parent');
function getArrayLi(array, parent) {
    for (let i = 0; i < array.length; i++) {
        let list = document.createElement('li');
        list.innerText = array[i];
        parent.append(list);
    }
}
getArrayLi(array, parent);
