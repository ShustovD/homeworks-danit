// Теоретичні питання:
// 1. Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування?
// Екранування в мовах програмування полягає у додаванні спецсимволів перед символами або рядками для запобігання їх некоректної інтерпретації.
// 2. Які засоби оголошення функцій ви знаєте?
// Function declaration, function expression, named function expression.
// 3. Що таке hoisting, як він працює для змінних та функцій?
// Hoisting — це механізм JavaScript, в якому змінні та оголошення функцій, пересуваються
// вгору своєї області видимості перед тим, як код буде виконано.

// Практичне завдання:

function createNewUser() {
    const firstName = prompt('What is your name?');
    const lastName = prompt('What is your last name?');
    const birthday = prompt('What is your birthday? (format: dd.mm.yyyy)');
    const newUser = {
        firstName,
        lastName,
        birthday,
        getLogin: function () {
            return (this.firstName[0] + this.lastName).toLowerCase();
        },
        getAge: function () {
            let today = new Date();
            let todayYear = today.getFullYear();
            let userYear = +this.birthday.slice(6, 10);
            let age = todayYear - userYear;
            return age;
        },
        getPassword: function () {
            return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.slice(6, 10);
        },
    };
    return newUser;
}
const user = createNewUser();
console.log(user);
console.log(user.getLogin());
console.log(user.getAge());
console.log(user.getPassword());
