// ## Теоретичні питання
// 1. Опишіть своїми словами що таке Document Object Model (DOM)
// DOM - це об'єктна модель документа за допомогою якого можна взаємодіяти з веб-сторінкою.
// 2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?
// innerText:
// 1. Отримує та встановлює вміст у вигляді звичайного тексту.
// 2. Ми не можемо вставити теги HTML.
// 3. Він ігнорує пробіли.
// 4. Він повертає текст без тегу внутрішнього елемента.
// А innerHTML у свою чергу:
// 1. Отримує та встановлює вміст у форматі HTML.
// 2. Ми можемо вставити теги HTML.
// 3. Він повертає тег із внутрішнім тегом елемента.
// 3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
// querySelector(), querySelectorAll(), getElementById(), getElementsByClassName(), getElementsByTagName(),
// getElementsByName(), getElementsByTagNameNS(), перші два являються найкращими, оскільки інші вважаються застарілими.

// Практичне завдання:

const paragraphs = document.querySelectorAll('p');
paragraphs.forEach(paragraphs => {
    paragraphs.style.backgroundColor = '#ff0000';
});
const optionsList = document.getElementById('optionsList');
console.log(optionsList);
let parent = optionsList.parentElement;
console.log(parent);
let childs = optionsList.childNodes;
let nodeName = optionsList.nodeName;
let nodeType = optionsList.nodeType;
console.log(nodeName, nodeType);
const testParagraph = document.getElementById('testParagraph');
testParagraph.innerText = 'This is a paragraph';
const mainHeader = document.querySelector('.main-header');
let mainHeaderChilds = mainHeader.childNodes;
console.log(mainHeaderChilds);
for (element of mainHeaderChilds) {
    element.className = 'nav-item';
}
const sectionTitle = document.querySelectorAll('.section-title');
sectionTitle.forEach(element => element.classList.remove('section-title'));
