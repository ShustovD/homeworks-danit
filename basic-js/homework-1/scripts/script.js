// Теоретичні завдання:
// 1. Як можна оголосити змінну у Javascript?
// Змінну у js можна оголосити за допомогою:
// - var;
// - let;
// - const.
// 2. У чому різниця між функцією prompt та функцією confirm?
// Функція prompt відкриває перед користувачем модальне вікно з текстовим полем, в яке користувач може внести
// певний текст та натиснути "ОК" чи "Скасувати", при цьому повертає цей текст або null, якщо натиснута кнопка "Скасувати"
// або клавіша Esc, тоді як confirm відкриває перед користувачем модальне вікно без текстового поля, з
// можливістю натискання "ОК" чи "Скасувати", при цьому повертає: "ОК" - "True", "Скасувати" - "False".
// 3. Що таке неявне перетворення типів? Наведіть один приклад.
// Це автоматичне перетворення типу, що виконується компілятором.
// let value
// value = 1 * 'qwerty';
// console.log(value);
// console.log(typeof value)

// Практичні завдання:
// 1. Оголосіть дві змінні: admin та name. Встановіть ваше ім'я в якості значення змінної name.
// Скопіюйте це значення в змінну admin і виведіть його в консоль.
let admin;
let name;
name = 'Ivan';
admin = name;
console.log(admin);

// 2. Оголосити змінну days і ініціалізувати її числом від 1 до 10. Перетворіть це число на кількість секунд і виведіть на консоль.
let days = 4;
const dayInSec = 24 * 60 * 60;
console.log(days * dayInSec);

// 3. Запитайте у користувача якесь значення і виведіть його в консоль.
let userName = prompt('Як тебе звати?');
alert(userName);
console.log(userName);
