// Теоретичні питання:
// 1. Опишіть своїми словами різницю між функціями setTimeout() і setInterval().
// setTimeout() дозволяє виконати функцію один раз, а setInterval() в свою чергу дозволяє виконувати функції регулярно(циклічно).
// 2. Що станеться, якщо в функцію setTimeout() передати нульову затримку? Чи спрацює вона миттєво і чому?
// Якщо передати в функцію нульову затримку, то вона виконається миттєво одразу після виконання поточного коду, тому що перевірка
// календаря виконується тільки після завершення поточного коду.
// 3. Чому важливо не забувати викликати функцію clearInterval(), коли раніше створений цикл запуску вам вже не потрібен?
// setInterval() - не обробляється збирачем сміття, поки він існує - передана функція займає пам'ять, тому
// потрібно викликати clearInterval(), щоб прибрати посилання на функцію та очистити пам'ять.
const imgItems = document.querySelectorAll('.image-to-show');
const buttonStart = document.querySelector('#button-one');
const buttonStop = document.querySelector('#button-two');
let index = 0;
let paused = false;

function changeImg() {
    if (!paused) {
        const everyImg = imgItems[index++ % imgItems.length];
        imgItems.forEach(e => {
            e.classList.remove('active');
        });
        everyImg.classList.add('active');
    }
}

changeImg();

setInterval(changeImg, 1000);

buttonStart.addEventListener('click', () => {
    paused = false;
});
buttonStop.addEventListener('click', () => {
    paused = true;
});
