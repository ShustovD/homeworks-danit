// Теоретичні питання:
// 1. Описати своїми словами навіщо потрібні функції у програмуванні.
// Функції в програмуванні потрібні для того щоб не прописувати наново один і той самий фрагмент коду в різних частинах программи.
// 2. Описати своїми словами, навіщо у функцію передавати аргумент.
// Для того щоб не створювати змінні let чи const у функцію можна вписати аргумент і js автоматично створить з неї локальну змінну.
// 3. Що таке оператор return та як він працює всередині функції?
// При виклику оператора return в функції він завершує її виконання та повертає її значення.

let firstNumber = Number(prompt('Enter first number.'));
let secondNumber = Number(prompt('Enter second number.'));
let mathOperation = prompt('Choose math operation (+,-,/,*)');
function calcFunction(firstNumber, secondNumber, mathOperation) {
    switch (mathOperation) {
        case '+':
            return Number(firstNumber) + Number(secondNumber);
        case '-':
            return firstNumber - secondNumber;
        case '/':
            return firstNumber / secondNumber;
        case '*':
            return firstNumber * secondNumber;
    }
}
console.log(calcFunction(firstNumber, secondNumber, mathOperation));
