const eyeTarget = document.querySelectorAll('#eyeTarget');
const firstPassword = document.querySelector('#input-password-first');
const secondPassword = document.querySelector('#input-password-second');
const message = document.querySelector('.message');
const button = document.querySelector('.btn');

eyeTarget.forEach(function (eye) {
    eye.onclick = function () {
        let target = this.getAttribute('data-target');
        inputPass = document.querySelector(target);
        if (inputPass.getAttribute('type') === 'password') {
            inputPass.setAttribute('type', 'text');
            eye.classList.toggle('fa-eye-slash');
        } else {
            inputPass.setAttribute('type', 'password');
            eye.classList.toggle('fa-eye-slash');
        }
    };
});

button.addEventListener('click', function (btn) {
    btn.preventDefault();
    if (firstPassword.value !== secondPassword.value) {
        message.innerHTML = 'Потрібно ввести однакові значення';
    } else if (firstPassword.value.length < 8 && secondPassword.value.length < 8) {
        message.innerHTML = 'Ваш пароль повинен містити 8 символів';
    } else if (firstPassword.value.search(/[a-z]/) < 0 && secondPassword.value.search(/[a-z]/) < 0) {
        message.innerHTML = 'Ваш пароль повинен містити одну маленьку букву';
    } else if (firstPassword.value.search(/[A-Z]/) < 0 && secondPassword.value.search(/[A-Z]/) < 0) {
        message.innerHTML = 'Ваш пароль повинен містити одну велику букву';
    } else if (firstPassword.value.search(/[0-9]/) < 0 && secondPassword.value.search(/[0-9]/) < 0) {
        message.innerHTML = 'Ваш пароль повинен містити одну цифру';
    } else if (firstPassword.value.includes(' ') && secondPassword.value.includes(' ')) {
        message.innerHTML = 'Ваш пароль не повинен містити пробіл';
    } else {
        alert('You are welcome!');
    }
});
