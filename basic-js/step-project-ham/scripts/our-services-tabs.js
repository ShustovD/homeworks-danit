const tab = function () {
    let tabTitle = document.querySelectorAll('.second-background-tabs-title'),
        tabContent = document.querySelectorAll('.second-background-tabs-content'),
        tabName;

    tabTitle.forEach(item => {
        item.addEventListener('click', showTabTitle);
    });
    function showTabTitle() {
        tabTitle.forEach(item => {
            item.classList.remove('active');
        });
        this.classList.add('active');
        tabName = this.getAttribute('data-tab');
        showTabContent(tabName);
    }

    function showTabContent(tabName) {
        tabContent.forEach(item => {
            item.classList.contains(tabName) ? item.classList.add('active') : item.classList.remove('active');
        });
    }
};

tab();
