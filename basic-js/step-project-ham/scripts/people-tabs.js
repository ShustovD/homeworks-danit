const tabPeople = function () {
    let tabTitle = document.querySelectorAll('.sixth-background-content-img-background'),
        tabContent = document.querySelectorAll('.sixth-background-content'),
        tabName;

    tabTitle.forEach(item => {
        item.addEventListener('click', showTabTitle);
    });
    function showTabTitle() {
        tabTitle.forEach(item => {
            item.classList.remove('active');
        });
        this.classList.add('active');
        tabName = this.getAttribute('data-tab-people');
        showTabContent(tabName);
    }

    function showTabContent(tabName) {
        tabContent.forEach(item => {
            item.classList.contains(tabName) ? item.classList.add('active') : item.classList.remove('active');
        });
    }
};

tabPeople();
