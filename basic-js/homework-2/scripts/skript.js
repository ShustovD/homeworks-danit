// Теоретичні питання:

// 1. Які існують типи даних у Javascript?
// У js існує 8 типів данних:
// - Number;
// - BigInt;
// - String;
// - Boolean;
// - Object;
// - Symbol;
// - null;
// - undefined.
// 2. У чому різниця між == і ===?
// Оператор "==" ігнорує тип данних змінної, а оператор "===", навпаки, перевіряє тип данних.
// 3. Що таке оператор?
// Оператор - це фунцкія за допомогою якої, ми запускаємо ту чи іншу вбудовану фунцкію js, яка виконує якісь дії і повертає результат.

// Практичне завдання:
let userName = prompt('What is your name?');
let userAge = Number(prompt('How old are you?'));

while (!userName || /\d/g.test(userName) || !userAge || isNaN(userAge)) {
    if (!userName) {
        userName = prompt('You don`t enter your name, try again.', userName) || userName;
    }

    if (/\d/g.test(userName)) {
        userName = prompt('Invalid user name, try again', userName) || userName;
    }

    if (!userAge) {
        userAge = Number(prompt('You don`t enter your age.', userAge)) || userAge;
    }

    if (isNaN(userAge)) {
        userAge = Number(prompt('Invalid user age, try again.', userAge)) || userAge;
    }
}

if (userAge < 18) {
    alert('You are not allowed to visit this website.');
} else if (userAge <= 22) {
    let userAgree = confirm('Are you sure you want to continue?');
    if (!!userAgree) {
        alert(`Welcome, ${userName}`);
    } else {
        alert('You are not allowed to visit this website.');
    }
} else {
    alert(`Welcome, ${userName}`);
}
